
import { Query } from "@xata.io/client";
import { xata } from "../lib/xata";
import type { ObjectsRecord } from "../lib/xata.codegen.server";

async function getPosts(search:string,filter:string){
    if( search ==="" && filter !="All"){ //cas où on a aucune recherche mais on veut une loc précise
       const res = await xata.db.objects.search(filter); 
       
       let data = [] //on fait le filtre des données nous même pour pouvoir afficher uniquement tout ceux d'un même batiment 
      
        for( let inc of res.records){
            if(inc.Localisation.substring(0,3) === filter){
                data.push(inc);
            }
        }

        return data;
    }
    else if ( search ==="" && filter ==="All"){ //cas général ou il y a aucun filtre
        //aucun traitement à faire en plus 
        const data = await xata.db.objects.getAll();
        return data;
    }
    else if(search !="" && filter !="All"){//cas où on a un search et un filtre 
        const res =await xata.db.objects.search(search,
            {fuzziness:1}); 

        let data = [] //on fait le filtre des données nous même pour pouvoir afficher uniquement tout ceux d'un même batiment 
      
        for( let inc of res.records){
            if(inc.Localisation.substring(0,2) === filter){
                data.push(inc);
            }
            if(inc.Localisation === "L/B" && filter ==="L/B"){ //comme on a 3 et pas 2 caractères
                data.push(inc);
            }
        }

        return data;
    
    }
    else if(search !="" && filter==="All"){
        const data =await xata.db.objects.search(search,
        {fuzziness:1}); 
        
        return data.records;
    }


    
}


export const get = async (search:string, filter:string) =>{
    //const oui = (await request.text()).toString() //on récupère le body de la requete POST passée avec fetch
    /*const data = JSON.parse(oui)//on la convertit en string et on parse le contenu pour récupérer le dictionnaire
    const filtre = data["filter"];
    const search = data["search"]*/
  
    return new Response(JSON.stringify(await getPosts(search,filter)),{
        status:200,
        headers:{
            "Content-Type" : "applications/json",
        }
    })
}