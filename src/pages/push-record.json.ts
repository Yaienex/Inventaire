import type { APIRoute } from "astro";
import { xata } from "../lib/xata";




export const post: APIRoute = async ({request}) => {
  const data_notparse = (await request.text()).toString() //on récupère le body de la requete POST passée avec fetch
  const data = JSON.parse(data_notparse)//on la convertit en string et on parse le contenu pour récupérer le dictionnaire
  const nom = data["nom"];
  const loc = data["loc"];
  const nb = data["nb"];


  await xata.db.objects.create({
    Name: nom,
    Localisation: loc,
    Nombres: nb,
  }


    
   
  );

  return new Response(null, {
    status: 200,
  });
};