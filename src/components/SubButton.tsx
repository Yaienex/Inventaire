export default function AddButton(props: { id: string }) {
    const onClick = () => {
      fetch(`api/sub-record/${props.id}.json`, {
        method: "delete",
      }).then(() => window.location.reload())
    }
  
    return <button onClick={onClick}>
      <span role="img" aria-label="moins"> - </span>
    </button>
  }