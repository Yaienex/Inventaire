

export default function AddButton(props: { id: string }) {
    const onClick = () => {
      fetch(`api/add-record/${props.id}.json`, {
        method: "delete",
      }).then(() => window.location.reload())
    }
  
    return <button onClick={onClick}>
      <span role="img" aria-label="plus"  > + </span>
    </button>
  }