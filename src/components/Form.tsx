import { useState } from "react"
import { WHERE } from "../const"


export default function Form (){
    const [nom, setName] =useState("")
    const [loc, setLoc] = useState("U1")//valeur par défaut au U1 au cas où
    const [nb, setNb] = useState(0)

   

    
    
   const  send= async ()=> {
      
      fetch(`push-record.json`,{
        method:"POST",
        headers: {
          "Accept" : "application/json",
          "Content-Type" : " application/json"
        },
        body: JSON.stringify({
          nom : nom,
          loc : loc,
          nb : nb ,
    
        })
      }).then(() => window.location.reload())
    }
    
    

    return  ( <> 
    
    <form onSubmit={e => {
      e.preventDefault();
      send();
    }} className="flex  flex-col">
    
     <div className="flex place-content-between space-y-2 w-full flex-col pt-10">
        <div  key="name">
          Nom de l'objet : 
          <input type="text" id="name" value={nom} onChange={e=> setName(e.target.value)} required />
        </div>
        <div  key="loc">
          Localisation de l'objet:
          <select value={loc} onChange={e => (setLoc(e.target.value ))}>
          {WHERE.map((loca) => (
              <option key={loca}> {loca}</option>
          ))}
          
          </select>
        </div>
        <div  key="numbers">
          Nombres :
          <input type="number" id="nb"value={nb} onChange={e => setNb(e.target.valueAsNumber)} required />
        </div>
        </div>
       
       
        <button className="">Enregistrer</button>
       
      </form>
      
     
      </>
    );
  }


//pour faire des locs plus détaillés faire des optgroup en respécifiant U7 : U7-1 | ATTENTION pour la search il faut
/* récupérer seulement les deux ou trois premiers caractères */